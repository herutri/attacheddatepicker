/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attacheddatepicker;

import java.time.LocalDate;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author Heru Tri Julianto <heru.trijulianto@panemu.com>
 */
public class AttachedDatepicker extends Application {
	
	@Override
	public void start(Stage primaryStage) {
		DatePicker datePicker = new DatePicker();
		attachShortcut(datePicker); //attachShortcut on datepicker

		VBox root = new VBox();
		root.getChildren().add(datePicker);

		Scene scene = new Scene(root, 300, 250);

		primaryStage.setTitle("Attach Shortcut DatePicker");
		primaryStage.setScene(scene);
		primaryStage.show();	
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}
	
	private void attachShortcut(DatePicker datePicker) {

		datePicker.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {

			LocalDate value = datePicker.getValue();
			if (value == null) {
				//give default value if there is no one
				value = LocalDate.now();
			}

			if (event.getCode() == KeyCode.UP && event.isControlDown()
					&& event.isShiftDown()) {
				//UP pressed, the control and shift are hold down, increase the year
				datePicker.setValue(value.plusYears(1));
			} else if (event.getCode() == KeyCode.UP && event.isControlDown()) {
				//UP pressed, the control key is hold down, increase the month
				datePicker.setValue(value.plusMonths(1));
			} else if (event.getCode() == KeyCode.UP) {
				//UP pressed, no modifier keys hold down, increase the day
				datePicker.setValue(value.plusDays(1));
			} else if (event.getCode() == KeyCode.DOWN && event.isControlDown() && event.isShiftDown()) {
				//DOWN pressed, the control and shift are hold down, decrease the year                      
				datePicker.setValue(value.minusYears(1));
			} else if (event.getCode() == KeyCode.DOWN && event.isControlDown()) {
				//DOWN pressed, the control key is hold down, decrease the month
				datePicker.setValue(value.minusMonths(1));
			} else if (event.getCode() == KeyCode.DOWN) {
				// DOWN pressed, no modifier keys hold, decrease the day
				datePicker.setValue(value.minusDays(1));
			}
		});
	}
	
	
}
